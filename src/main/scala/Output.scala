import scala.swing._
import scala.concurrent.duration._
import rx.lang.scala._
import rx.lang.scala.subjects.PublishSubject
import scala.swing.event.Event
import javax.swing.JPanel
import java.awt.Graphics
import java.awt.Color
import javax.swing.Box

object Output {

  def print(grid: Grid) {
    println()
    println(grid.reduce((alive, pos, prev: String) => {
      val (x, y) = pos
      prev +
        (if (x == 0) "\n" else "") +
        (if (x != 0 || y % 2 == 1) " " else "") +
        (if (alive) "X" else ".")
    }, ""))
  }

  def console(printn: Int) = {
    var step: Int = 0
    (grid: Grid) => {
      step += 1
      if (step == printn) {
        step = 0
        print(grid)
      }
    }
  }

  class HexDisplay extends JPanel {
    private val xScale = 17
    private val yScale = 16
    private val margin = 8

    var grid: Grid = Grid.ofSize(0)
    setOpaque(true)
    setFont(getFont().deriveFont(22.0f))

    def showGrid(g: Grid) {
      grid = g
      setSize(g.size * xScale + 2 * margin,
              g.size * yScale + 2 * margin)
      repaint()
    }

    override def getPreferredSize(): Dimension =
      new Dimension(grid.size * xScale + 2 * margin,
                    grid.size * yScale + 2 * margin)

    override def paintComponent(g: Graphics) {
      super.paintComponent(g)
      g.setFont(getFont())
      grid.reduce((alive, pos, prev: Int) => {
        val (x,y) = pos
        g.setColor(if (alive) Color.BLACK else Color.WHITE) 
        val xPos = x * xScale + ((y % 2) * xScale / 2) + margin
        val yPos = (y + 1) * yScale + margin
        g.drawString("\u2B22", xPos, yPos)
        prev
      }, 0)
    }
  }

  def graphical() = {
    // signals
    val nextGrid = PublishSubject[Grid]
    val grids = nextGrid.scan(List.empty[Grid])((lst, g) => lst :+ g)
    val frameInput = PublishSubject[Int]
    val frame = frameInput.distinctUntilChanged
    val currentGrid: Observable[Grid] = grids.combineLatest(frame).map(pair => pair._1(pair._2))

    // components
    val display = new HexDisplay()

    val slider: Slider = new Slider { min = 0; value = 0 }
    slider.subscribe({ case _ => frameInput.onNext(slider.value) })

    val label = new Label

    // updates
    grids.subscribe(lst => slider.max = 0 max lst.size - 1)
    currentGrid.subscribe(g => display.showGrid(g))
    frame.subscribe(f => label.text = s"Count: $f")

    new SimpleSwingApplication {
      def top = new MainFrame {
        title = "Hexagonal Game of Life"
        contents = new BoxPanel(Orientation.Vertical) {
          contents += Component.wrap(display)
          peer.add(Box.createVerticalStrut(2))
          contents += new BoxPanel(Orientation.Horizontal) {
            contents += slider
            contents += label
            border = Swing.EmptyBorder(30)
          }
          border = Swing.EmptyBorder(30)
        }
      }
    }.main(Array())
    (grid: Grid) => nextGrid.onNext(grid)
  }
}