import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import org.scalacheck.Prop.propBoolean
import org.scalacheck.Prop.throws

object GridSpecification extends Properties("Grid") {

  // Grid generation

  def numberOfLiveCells(g: Grid): Int =
    g.reduce((alive, pos, prev: Int) => if (alive) prev + 1 else prev, 0)

  property("zeroInit") =
    numberOfLiveCells(Grid.ofSize(100).random(0.0)) == 0

  property("oneInit") =
    numberOfLiveCells(Grid.ofSize(100).random(1.0)) == 100 * 100

  // Grid neighbors

  val grid3 = Grid.ofSize(3)

  property("topLeft") = grid3.neighbors((0, 0)).size == 2

  property("topCentral") = grid3.neighbors((1, 0)).size == 4

  property("topRight") = grid3.neighbors((2, 0)).size == 3

  property("centralLeft") = grid3.neighbors((0, 1)).size == 5

  property("central") = grid3.neighbors((1, 1)).size == 6

  property("centralRight") = grid3.neighbors((2, 1)).size == 3

  property("bottomLeft") = grid3.neighbors((0, 2)).size == 2

  property("bottomCentral") = grid3.neighbors((1, 2)).size == 4

  property("bottomRight") = grid3.neighbors((2, 2)).size == 3

  property("topLeft12") = grid3.secondNeighbors((0, 0)).size == 2

  property("topCentral12") = grid3.secondNeighbors((1, 0)).size == 2

  property("leftCentral12") = grid3.secondNeighbors((0, 1)).size == 2

  property("central12") = grid3.secondNeighbors((1, 1)).size == 2

  // Load tests

  property("boolArray") = forAll { (b: Array[Boolean]) =>
    b.size < 9 || b.deep == grid3.load(b.toList).asBoolArray()
  }

  property("strings") = {
    val d:List[Boolean] = List(
      true,        false,        false,
             true,        false,        false,
      true,       false,        true
    )
    val strings: List[String] = List(
      "X..",
      "X..",
      "X.X"
    )
    d.toArray.deep == grid3.loadLines(strings).asBoolArray().deep
  }

  property("stringsExtra") = {
    val d:List[Boolean] = List(
      true,        false,        false,
             true,        false,        false,
      true,       false,        true
    )
    val strings: List[String] = List(
      "X..",
      "X..X.X...",
      "X.X",
      "XXXXXX"
    )
    d.toArray.deep == grid3.loadLines(strings).asBoolArray().deep
  }

  property("missingLines") = throws(classOf[RuntimeException]) {
    grid3.loadLines(List(
      "X..",
      "X.."
    ))
  }

  property("missingChars") = throws(classOf[RuntimeException]) {
    grid3.loadLines(List(
      "X..",
      "X.",
      "X.X"
    ))
  }

  property("wrongChars") = throws(classOf[RuntimeException]) {
    grid3.loadLines(List(
      "X..",
      "ABC",
      "X.X"
    ))
  }

}