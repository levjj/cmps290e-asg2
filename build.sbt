import AssemblyKeys._

lazy val root = (project in file(".")).
  settings(assemblySettings: _*).
  settings(
    name := "gol6",
    version := "1.0",
    scalaVersion := "2.10.4",
    libraryDependencies += "com.github.scopt" %% "scopt" % "3.3.0",
    libraryDependencies += "org.scala-lang" % "scala-swing" % "2.10.2",
    libraryDependencies += "io.reactivex" %% "rxscala" % "0.25.1",
    libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.12.5" % "test",
    resolvers += Resolver.sonatypeRepo("public"),
    resolvers += Resolver.sonatypeRepo("releases")
  )
