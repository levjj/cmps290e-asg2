import java.io.File
import scopt.OptionParser
import scala.io.Source

case class Config(
  rule12: Boolean = false,
  size: Int = 100,
  file: File = null,
  generations : Int = 10,
  printn : Int = 1,
  prob : Double = 0.5,
  gui : Boolean = false)

object Main {

  def run(config: Config) {
    val sim = if (config.rule12) Rules.rule12 else Rules.rule6
    val out = if (config.gui) Output.graphical else Output.console(config.printn)
    var grid: Grid = Grid.ofSize(config.size)
    grid = if (config.file == null) {
      grid.random(config.prob)
    } else {
      grid.loadLines(Source.fromFile(config.file).getLines.toList)
    }
    out(grid)
    for (_ <- 1 to config.generations) {
      grid = grid.map(sim)
      out(grid)
    }
  }

  def main(args: Array[String]) = {
    val parser = new scopt.OptionParser[Config]("gol6") {
      opt[Unit]("12") action { (_, c) => c.copy(rule12 = true) } text("use the 12 neighbor rules (default is the 6 neighbor rules)")
      opt[Int]("size") action { (x, c) => c.copy(size = x) } text("specify the size of the grid to be n X n") valueName("N")
      opt[File]('f', "file") action { (x, c) => c.copy(file = x) } text("read in the initial configuration from the specified file") valueName("FILENAME")
      opt[Int]('g', "generations") action { (x, c) => c.copy(generations = x) } text("specifiy the number of generations to simulate") valueName("N")
      opt[Int]('p', "printn") action { (x, c) => c.copy(printn = x) } text("specify that every nth generation should be printed (for plain text output only)") valueName("N")
      opt[Double]('i', "prob") action { (x, c) => c.copy(prob = x) } text("specify the probabilty of a cell being alive in the initial configuration if no file is provided") valueName("P")
      opt[Unit]('u', "gui") action { (_, c) => c.copy(gui = true) } text("show a graphical user interface")
      help("help") text("prints this usage text")
    }
    parser.parse(args, Config()) match {
      case Some(config) => run(config)
      case None => ()
    }
  }
}