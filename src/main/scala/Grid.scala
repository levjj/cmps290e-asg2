import scala.util.Random
import java.util.function.Predicate
import java.util.regex.Pattern

case class Grid(val data: Array[Array[Boolean]]) {

  def reduce[R](f: (Boolean, (Int, Int), R) => R, initial: R) = {
    var result: R = initial;
    for (y <- 0 until data.length) {
      for (x <- 0 until data(y).length) {
        result = f(data(x)(y), (x, y), result)
      }
    }
    result
  }

  def map(f: (Boolean, (Int, Int), Grid) => Boolean):Grid =
    reduce((alive, pos, grid: Grid) => {
      val (x,y) = pos
      grid.data(x)(y) = f(alive, pos, this)
      grid
    }, Grid.ofSize(data.length))

  def apply(pos: (Int, Int)) = {
    val (x,y) = pos
    data(x)(y)
  }

  def size = data.length

  def random(prob: Double) = map((_, _, _) => Random.nextDouble() < prob)

  def load(lst: List[Boolean]) = map((_, pos, _) => lst(pos._1 + pos._2 * data.length))

  val LINE_FORMAT: Predicate[String] = Pattern.compile("^[X\\.]+$").asPredicate()

  def loadLines(lines: List[String]): Grid = {
    if (lines.size < data.length ||
        lines.exists(s => s.length() < data.length || !LINE_FORMAT.test(s))) {
      throw new RuntimeException("Input configuration has wrong size/format!")
    }
    load(lines.map(_.substring(0, data.length)) // truncate lines
              .flatMap(s => s.map(_ == 'X')))   // X = alive
  }

  def neighbors(pos: (Int,Int)) = {
    val (x,y) = pos
    List(
      // left
      (x - 1, y),
      // top
      (x, y - 1),
      // right
      (x + 1, y),
      // bottom
      (x, y + 1),
      // top left or top right
      if (y % 2 == 0) (x - 1, y - 1) else (x + 1, y - 1),
      // bottom left or bottom right
      if (y % 2 == 0) (x - 1, y + 1) else (x + 1, y + 1)
    ).filter(pos => { val (x,y) = pos; x >= 0 && x < data.length && y >= 0 && y < data.length})
  }

  def secondNeighbors(pos: (Int, Int)) = {
    val (x,y) = pos
    List(
      // top
      (x, y - 2),
      // bottom
      (x, y + 2),
      // top left
      if (y % 2 == 0) (x - 2, y - 1) else (x - 1, y - 1),
      // bottom left
      if (y % 2 == 0) (x - 2, y + 1) else (x - 1, y + 1),
      // top right
      if (y % 2 == 0) (x + 1, y - 1) else (x + 2, y - 1),
      // bottom right
      if (y % 2 == 0) (x + 1, y + 1) else (x + 2, y + 1)
    ).filter(pos => { val (x,y) = pos; x >= 0 && x < data.length && y >= 0 && y < data.length})
  }

  def asBoolArray(): Array[Boolean] = reduce((alive, pos, prev: Array[Boolean]) => {
    val (x,y) = pos
    prev(x + y * data.length) = alive
    prev
  }, Array.ofDim[Boolean](data.length * data.length))
}

object Grid {
  def ofSize(size: Int) = new Grid(Array.ofDim[Boolean](size, size)) 
}