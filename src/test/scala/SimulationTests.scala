import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import org.scalacheck.Prop.propBoolean
import org.scalacheck.Prop.throws

object SimulationSpecification extends Properties("Simulation") {

  val grid: Grid = Grid.ofSize(3) 

  property("rule6") = {
    val actual = grid.load(List(
      true,        false,        false,
             false,        false,        false,
      false,       false,        false
    )).map(Rules.rule6).asBoolArray()
    val expected = Array(
      false,        false,        false,
             false,        false,        false,
      false,       false,        false
    )
    expected == actual
  }

  property("rule12") = {
    val actual = grid.load(List(
      false,        false,        false,       false,        false,
             false,        false,        true,        true,          false,
       false,        false,        true,        true,        false,
             false,        true,        false,        true,          false,
       false,        false,        false,       false,        false
    )).map(Rules.rule6).asBoolArray()
    val expected = Array(
      false,        false,        false,       true,        false,
             false,        true,         true,        true,          false,
       false,        true,         false,        false,       false,
             false,        false,        false,        false,        false,
      false,        false,        false,       false,       false
    )
    expected == actual
  }
}