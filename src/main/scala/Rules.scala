object Rules {
  val rule6 = (alive: Boolean, pos: (Int, Int), grid: Grid) => {
    val liveNeighbors = grid.neighbors(pos).map(p => if (grid(p)) 1 else 0).reduce(_+_)

    if (alive) {
      // Any live cell with fewer than two live neighbors dies, as if caused by under-population.
      // Any live cell with two or three live neighbors lives on to the next generation.
      // Any live cell with more than three live neighbors dies, as if by overcrowding.
      liveNeighbors == 2 || liveNeighbors == 3
    } else {
      // Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
      liveNeighbors == 3
    }
  }

  val rule12 = (alive: Boolean, pos: (Int, Int), grid: Grid) => {
    val firstNeighbors = grid.neighbors(pos).map(p => if (grid(p)) 1.0 else 0.0)
    val secondNeighbors = grid.secondNeighbors(pos).map(p => if (grid(p)) 0.3 else 0.0)
    val liveNeighbors = (firstNeighbors ++ secondNeighbors).reduce(_+_)

    if (alive) {
      // A live cell survives to the next generation if this sum falls within the range of 2.0 - 3.3.
      //  Otherwise it dies (becomes an empty space).
      liveNeighbors >= 2.0 && liveNeighbors <= 3.3
    } else {
      // A cell is born into an empty space if this sum falls within the range of 2.3 - 2.9
      liveNeighbors >= 2.3 && liveNeighbors <= 2.9
    }
  }
}